>[!summary] WP5 Inhaltsangabe
>- [[1) Diskrete Signale]]
>- [[2) z-Transformation]]
>- [[3) Diskrete Systeme]]

## Literatur
- T. Frey, M. Bossert: Signal- und Systemtheorie, Teubner-Verlag
