## 1. [[Vorlesung_Signale und Systeme_WP1.pdf#page=4|Signale im Zeitbereich, Faltung]]
### [[Idealisierte Signale]]
- [[Idealisierte Signale#[Einheitsimpuls](WP1_Lerneinheit2_Kurzzusammenfassung.pdf page=4)|Einheitsimpuls]]
- [[Idealisierte Signale#[Einheitssprung](WP1_Lerneinheit2_Kurzzusammenfassung.pdf page=7)|Einheitssprung]]

### [[lineare und zeitinvariante Systeme]]
- [[lineare und zeitinvariante Systeme#[Zeitinvarianz](WP1_Lerneinheit1_Kurzzusammenfassung.pdf page=4)|Zeitinvarianz]]
- [[lineare und zeitinvariante Systeme#[Linearität](WP1_Lerneinheit1_Kurzzusammenfassung.pdf page=5)|Linearität]]
- [[lineare und zeitinvariante Systeme#[Kausalität ](WP1_Lerneinheit1_Kurzzusammenfassung.pdf page=6)|Kausalität]]

## 2. 