In der 12. Lerneinheit betrachten wir die Stabilität von realen LTI-Systemen (insbesondere von RLC-Netzwerken) im Laplace-Bereich. 
- Wir stellen fest, dass in realen RLC-Netzwerken die Übertragungsfunktionen (echte oder unechte) gebrochen-rationale Funktionen in $p$ sind. 
- Wir widmen uns der wichtigen Fragestellungen unter welchen Bedingungen RLC-Netzwerke BIBO stabil sind. 
- Wir beantworten die Frage wie die **Nullstellen** und die **Polstellen** der gebrochen-rationalen Übertragungsfunktion in der Laplace-Ebene angeordnet sein müssen damit ein endliches Eingangssignal garantiert zu einem endlichen Ausgangssignal führt - der **Ausgang also nicht** beliebig, d.h. **ohne Begrenzung verstärkt** wird.

Die Fragestellung der Stabilität spielt in Regelungstechnik eine große Rolle.
>[!important] Ziel ist es Systeme so zu entwerfen, dass der Systemausgang innerhalb einer vorgegebenen Zeit gegen einen gewünschten Wert konvergiert bzw. einen gewünschten Verlauf annimmt.

>[!linfo] [[Systemanalyse im Zeitbereich#BIBO Stabilität|Rückblick]] 
>- Ein LTI-System ist BIBO stabil, wenn die Impulsantwort absolut integrierbar ist (hinreichend).
>- Aus $|x(t)| ≤ ∞$ folgt dann, dass $|y(t)| ≤ ∞$. Ein begrenzter Eingang führt zu einem begrenzten Ausgang (bounded-input bounded-output).
>- Für BIBO stabile Systeme existiert die Fourier-Transformierte, d.h., $|H(jω)| < ∞$.
>>[!info] Eine notwendige Bedingung für die Stabilität eines Systems mit der Impulsantwort $h(t)$ ist, dass der Konvergenzbereich der Übertragungsfunktion $H(p) = \mathcal{L}\{h(t)\}$ die imaginäre Achse $(p = jω)$ beinhaltet.

>[!example]- Beispiel:    $H(p)=\frac{1}{(p-p_1)^{l_1}}$
>S.40-43
>![[Vorlesung_Laplace_WP4.pdf#page=40]]

## Fallunterscheidung
1. Alle Polstellen $p_i$ sind strikt in der linken Halbebene ($Re\{p_i\} < 0 ∀i$). 
	-  $⇒$ ***Das LTI-System ist stabil.***
2. Alle Polstellen sind in der linken Halbebene, allerdings existieren Polstellen auf der imaginären Achse für einige $i$. Wir unterscheiden zwei Fälle:
	1. Wenn alle Polstellen $p_i$ auf der imaginären Achse **einfach** (1. Ordnung) sind ($Re\{p_i\} = 0 ⇒ l_i = 1$), 
		- dann ist das System **grenzstabil** 
		- Die Impulsantwort lässt sich dann mithilfe der [[1) Fourier-Transformation#Rücktransformation in Zeitbereich (inverse Fourier-Transformation)|inversen Fourier-Transformation]] berechnen: $\mathcal{L}^{-1}\{\frac{1}{p-p_0}\}=\mathcal{F}^{-1}\{\frac{1}{j\omega - j\omega_i}\}$
	2. Wenn auf der imaginären Achse Polstellen $p_i$ **höherer Ordnung** ($l_i > 1$) existieren, dann ist das System **instabil**.
3. Es existieren Polstellen $p_i$ in der **rechten Halbebene** 
	- $⇒$ Das System ist **instabil**.

>[!info]- Graphik Polstellen stabiler Systeme
>![[Vorlesung_Laplace_WP4.pdf#page=45]]

Die oben genannten Kriterien für die Stabilität von LTI Systemen basieren auf den [[2) Beidseitige Laplace-Transformation#Konvergenz der beidseitigen Laplace-Transformation|Konvergenzeigenschaften]] der Laplace-Transformation

*Stabilitätskriterien für den Entwurf, den Betrieb und die Optimierung von LTI-Systemen werden in praktischen Anwendungen intensiv genutzt.*
