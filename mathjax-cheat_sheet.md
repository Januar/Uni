## Useful but hard to remember
### Calligraphic letters
$\mathcal{L}, \mathcal{F}$
### Zahlenräume
$\mathbb{N}, \mathbb{R}$
### Big Parentheses/Brackets/Braces
$$\left\lparen \frac{a}{b} \right\rparen,\left\lbrack \frac{a}{b} \right\rbrack, \left\lbrace \frac{a}{b} \right\rbrace, \left\lbrace \frac{\left\lparen \frac{a}{b} \right\rparen}{\sqrt{\frac{a}{b}}} \right\rbrace$$

